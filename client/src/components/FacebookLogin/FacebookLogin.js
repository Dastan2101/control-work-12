import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import Button from '@material-ui/core/Button';
import {connect} from "react-redux";
import {facebookLogin} from "../../store/actions/Actions";


const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    }
});

class FacebookLogin extends Component {

    facebookLogin = data => {
        if (data.error) {
        } else if (!data.name) {
            // passed cancel here also can use notification
        } else {
            this.props.facebookLogin(data)

        }
    };

    render() {
        return (
            <FacebookLoginButton
                appId="2732767663406625"
                callback={this.facebookLogin}
                fields="name,email,picture"
                render={renderProps => (
                    <Button variant="contained" color="primary" className={styles.button}
                            onClick={renderProps.onClick}>
                        Facebook login
                    </Button>

                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);