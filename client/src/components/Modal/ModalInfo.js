import React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import {apiURL} from "../../constants";


const ModalInfo = (props) => {
    return (
        <Dialog
            onClose={props.handleClose}
            aria-labelledby="customized-dialog-title"
            open={props.open}
            fullWidth={true}
            // fullScreen={true} мог бы сделать на весь экран, но смотрится не очень
        >
            <DialogContent>
                <img src={apiURL + '/uploads/' + props.image} alt="#"
                     style={{
                         backgroundSize: 'cover',
                         width: '100%',
                         height: 'auto',
                         margin: '0 auto',
                         display: 'block'
                     }}/>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.handleClose} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ModalInfo;