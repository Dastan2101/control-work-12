import React, {Component} from 'react';
import {deletePhoto, getUsersGallery} from "../../store/actions/Actions";
import {connect} from "react-redux";
import CardActionArea from "@material-ui/core/CardActionArea/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia/CardMedia";
import {apiURL} from "../../constants";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import Card from "@material-ui/core/Card/Card";
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import ModalInfo from "../../components/Modal/ModalInfo";

const styles = theme => ({
    card: {
        width: 300,
        marginBottom: 20,
        display: 'inline-block',
        margin: '0 50px'
    },
    media: {
        paddingTop: '56.25%',
        backgroundSize: 'curtain'
    },
    root: {
        flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    }
});

class MyGallery extends Component {
    state = {
        isOpen: false
    };

    componentDidMount() {
        this.props.getUsersGallery(this.props.match.params.id)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.usersGallery.length !== prevProps.usersGallery.length) {
            this.props.getUsersGallery(this.props.match.params.id)
        }
    }

    openImageModal = (image) => {
        this.setState({
            isOpen: true,
            image: image
        });
    };
    handleClose = () => {
        this.setState({isOpen: false});
    };

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                {this.props.usersGallery.map((item, ndx) => (
                    <Card
                        className={classes.card}
                        key={ndx}
                        >
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={apiURL + '/uploads/' + item.image}
                                onClick={() => this.openImageModal(item.image)}
                            /> <CardContent>
                            <Typography gutterBottom variant="h5" component="h2" color="primary">
                                {item.title}
                            </Typography>
                            <Typography gutterBottom variant="h5" component="h2" color="primary">
                                <b style={{color: 'green'}}>author:</b> {item.user ? item.user.displayName : <p>Not Found</p>}
                            </Typography>
                        </CardContent>
                        </CardActionArea>
                        {this.props.user && this.props.user._id === item.user._id &&
                        <Button variant="contained" color="secondary" className={classes.button}
                                onClick={() => this.props.deletePhoto(item._id)}
                        >
                            Delete
                            <DeleteIcon className={classes.rightIcon}/>
                        </Button>}
                    </Card>
                ))}
                <ModalInfo
                    open={this.state.isOpen}
                    handleClose={this.handleClose}
                    image={this.state.image}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    usersGallery: state.gallery.usersGallery,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    getUsersGallery: id => dispatch(getUsersGallery(id)),
    deletePhoto: id => dispatch(deletePhoto(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MyGallery));