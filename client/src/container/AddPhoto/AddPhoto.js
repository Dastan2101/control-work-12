import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField/TextField";
import {createGalleryPhoto} from "../../store/actions/Actions";

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    input: {
        display: 'none',
    }
});


class AddPhoto extends Component {

    state = {
        title: '',
        image: '',
        error: null
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    submitFormHandler = event => {
        event.preventDefault();

        if (this.state.title === '') {
            return this.setState({error: true})
        }
        if (this.state.image === '') {
            return this.setState({error: true})
        }
        const formData = new FormData();
        delete this.state.error;

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createGalleryPhoto(formData)

    };

    render() {
        const {classes} = this.props;

        return (
            <form className={classes.container} noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
                <TextField
                    label="Title"
                    className={classes.textField}
                    type="text"
                    name="title"
                    required
                    error={this.props.error && true}
                    helperText={this.state.error && 'All fields required'}
                    margin="normal"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                />
                <input
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    required
                    type="file"
                    name="image"
                    onChange={this.fileChangeHandler}
                />
                <label htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.button}>
                        Download image
                    </Button>
                </label>
                <Button variant="contained" color="secondary" size="small" className={classes.button}
                        type="submit">
                    <SaveIcon className={classes.leftIcon}/>
                    Save
                </Button>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createGalleryPhoto: data => dispatch(createGalleryPhoto(data))
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(AddPhoto));