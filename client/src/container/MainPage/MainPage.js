import React, {Component} from 'react';
import {getGallery} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {Link} from 'react-router-dom'
import * as WOW from "wow.js";

import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import {apiURL} from "../../constants";
import Button from "@material-ui/core/Button/Button";
import ModalInfo from "../../components/Modal/ModalInfo";


const styles = theme => ({
    card: {
        width: 300,
        marginBottom: 20,
        display: 'inline-block',
        margin: '0 50px'
    },
    media: {
        paddingTop: '56.25%',
        backgroundSize: 'curtain'
    }

});

class MainPage extends Component {
    state = {
        isOpen: false,
        image: null
    };

    openImageModal = (image) => {
        this.setState({
            isOpen: true,
            image: image
        });
    };
    handleClose = () => {
        this.setState({isOpen: false});
    };

    componentDidMount() {
        this.props.getGallery();
        new WOW().init();
    }

    render() {
        const {classes} = this.props;

        return (
            <div>

                {this.props.gallery.map((item, key) => (
                        <Card
                            className={classes.card}
                            key={key}
                            onClick={() => this.openImageModal(item.image)}>
                            <CardActionArea>
                                <CardMedia
                                    className={classes.media}
                                    image={apiURL + '/uploads/' + item.image}
                                /> <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" color="primary">
                                    {item.title}
                                </Typography>
                                <Button color="inherit" component={Link} to={`/gallery/my_gallery/${item.user._id}`}>
                                    created by: {item.user.displayName}
                                </Button>

                            </CardContent>
                            </CardActionArea>
                        </Card>
                ))

                }
                <ModalInfo
                    open={this.state.isOpen}
                    handleClose={this.handleClose}
                    image={this.state.image}
                />

            </div>

        );
    }
}

const mapStateToProps = state => ({
    gallery: state.gallery.gallery
});

const mapDispatchToProps = dispatch => ({
    getGallery: () => dispatch(getGallery())
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainPage));