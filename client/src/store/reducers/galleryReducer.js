import {
    FETCH_GALLERY_SUCCESS, FETCH_USERS_GALLERY,
} from "../actions/Actions";


const initialState = {
    gallery: [],
    usersGallery: []
};
const galleryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GALLERY_SUCCESS:
            return {...state, gallery: action.data};
        case FETCH_USERS_GALLERY:
            return {
                ...state, usersGallery: action.data
            };
        default:
            return state;
    }
};

export default galleryReducer;