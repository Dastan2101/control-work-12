import axios from '../../axios-api';
import {push} from "connected-react-router";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';

export const FETCH_TOKEN_SUCCESS = 'FETCH_TOKEN_SUCCESS';

export const FETCH_GALLERY_SUCCESS = 'FETCH_GALLERY_SUCCESS';

export const FETCH_USERS_GALLERY = 'FETCH_USERS_GALLERY';

const registerUserSuccess = () => ({type: REGISTER_USER_SUCCESS});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

const fetchToken = token => ({type: FETCH_TOKEN_SUCCESS, token});

const fetchGallery = (data) => ({type: FETCH_GALLERY_SUCCESS, data});

const fetchUsersGallery = data => ({type: FETCH_USERS_GALLERY, data});

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess());
                dispatch(push('/login'));
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(registerUserFailure(error.response.data));
                } else {
                    dispatch(registerUserFailure({global: 'No connection'}));
                }
            }
        );
    };
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                dispatch(fetchToken(response.data));
                dispatch(push('/'));
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(loginUserFailure(error.response.data));
                } else {
                    dispatch(loginUserFailure({global: 'No connection'}));
                }
            }
        );
    };
};

export const logoutUser = () => {
    return dispatch => {
        return axios.delete('/users/sessions').then(
            () => {
                dispatch({type: LOGOUT_USER});
            }
        )
    }
};

export const facebookLogin = (userData) => {
    console.log(userData);
    return dispatch => {
        axios.post('/users/facebookLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                dispatch(push('/'))
            },
            () => {
                dispatch(loginUserFailure('Login failed!'))
            }
        )
    }
};

export const createGalleryPhoto = (data) => {
    return dispatch => {
        return axios.post('/gallery/add', data).then(
            () => dispatch(push('/'))
        )
    }
};

export const getGallery = () => {
    return dispatch => {
        return axios.get('/gallery').then(
            response => dispatch(fetchGallery(response.data))
        )
    }
};

export const getUsersGallery = id => {
    return dispatch => {
        return axios.get('/gallery/my_gallery/' + id).then(
            response => dispatch(fetchUsersGallery(response.data))
        )
    }
};

export const deletePhoto = (id) => {
    return dispatch => {
        return axios.delete('/gallery/' + id).then(
            response => dispatch(fetchUsersGallery(response.data))
        )
    };
};