import React, {Component, Fragment} from 'react';
import {withRouter} from "react-router-dom";
import Navigation from "./components/Toolbar/Navigation";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/Actions";
import {Route, Switch} from "react-router-dom";
import MainPage from "./container/MainPage/MainPage";
import Login from "./container/Login/Login";
import Register from "./container/Register/Register";
import AddPhoto from "./container/AddPhoto/AddPhoto";
import MyGallery from "./container/MyGallery/MyGallery";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Navigation
                        user={this.props.user}
                        logoutUser={this.props.logoutUser}
                    />
                </header>
                <div style={{width: '1200px', margin: '50px auto'}}>
                    <Switch>
                        <Route path="/" exact component={MainPage}/>
                        <Route path="/gallery/my_gallery/:id" exact component={MyGallery}/>
                        <Route path="/registration" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/add" exact component={AddPhoto}/>
                    </Switch>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
