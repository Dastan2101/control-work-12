const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/cw_12',
    mongoOptions: {useNewUrlParser: true, useCreateIndex: true},
    facebook: {
        appId: '2732767663406625',
        appSecret: '03b07656d5aeccd291c2440b5c5f2970'
    }
};
