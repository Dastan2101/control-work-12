const express = require('express');
const Gallery = require('../models/Gallery');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.post('/add', [auth, upload.single('image')], async (req, res) => {
    try {
        const gallery = await new Gallery({
            user: req.user._id,
            title: req.body.title,
            image: req.file.filename
        });

        await gallery.save();

        res.send(gallery)
    } catch (error) {
        res.send({message: 'Fields required'}).status(500);
    }
});

router.get('/', (req, res) => {

    Gallery.find().populate('user', 'displayName')
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))

});

router.get('/my_gallery/:id', (req, res) => {

    Gallery.find({user: req.params.id}).populate('user', 'displayName')
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});

router.delete('/:id', auth, async (req, res) => {

    const gallery = await Gallery.findById(req.params.id);

    if (gallery) {
        gallery.remove();
        await gallery.save();
    }

    const galleries = await Gallery.find({user: req.user._id});

    res.send(galleries);

});

module.exports = router;