const nanoid = require('nanoid');

const mongoose = require('mongoose');
const config = require('./config');

const Gallery = require('./models/Gallery');
const User = require('./models/Users');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create({
            username: "Ivanov Ivan",
            password: "123",
            token: nanoid(),
            displayName: 'Ivanov Ivan',
            image: 'http://localhost:8000/uploads/Ivan.jpeg'
        },
        {
            username: "Richard Brake",
            password: "123",
            token: nanoid(),
            displayName: 'Night King',
            image: 'http://localhost:8000/uploads/Petya.jpeg'

        },
        {
            username: 'John Snow',
            password: '123',
            token: nanoid(),
            displayName: 'King of the North',
            image: 'http://localhost:8000/uploads/John.png'

        });

    await Gallery.create(
        {
            title: 'Free Sea & Ocean',
            image: 'image-1.jpg',
            user: user[0]._id
        },
        {
            title: 'Best Way',
            image: 'image-2.jpg',
            user: user[0]._id
        },
        {
            title: 'Beautiful Natural',
            image: 'image-3.jpeg',
            user: user[0]._id
        },
        {
            title: 'Forest Landscape',
            image: 'image-4.jpg',
            user: user[1]._id
        },
        {
            title: 'Cascada Munte',
            image: 'image-5.jpg',
            user: user[1]._id
        },
        {
            title: 'Looking Very Well',
            image: 'image-7.jpeg',
            user: user[2]._id
        },
        {
            title: 'Discover & Waterfall',
            image: 'image-6.jpg',
            user: user[2]._id
        }
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});